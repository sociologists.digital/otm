{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/21.11.tar.gz") {} }:

with pkgs;

stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    direnv
    python3.pkgs.mistune
    python3.pkgs.requests
    python3.pkgs.smartypants
    python3.pkgs.staticjinja
    python3.pkgs.tomli
  ];
}
