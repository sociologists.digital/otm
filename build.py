import os
from hashlib import md5
from datetime import datetime
from functools import partial

import requests
import tomli
from mistune import markdown
from smartypants import smartypants
from staticjinja import Site


def get_data():
    sheet = os.environ["SHEET"]
    user = os.environ["USER"]
    password = os.environ["PASS"]
    columns = ["timestamp", "name", "email", "affiliation", "bio", "url", "confirm"]
    r = requests.get(sheet, auth=(user, password))
    r.raise_for_status()
    j = r.json()
    return [dict(zip(columns, i.values())) for i in j]


def profile_slug(name):
    slug = name.strip().lower().replace(" ", "-").replace(",", "").replace(".", "")
    return slug


def render_markdown(md):
    return smartypants(markdown(md, escape=False))


def gravatar(email, size=200, alt="blank"):
    mailhash = md5(bytes(email.lower().strip(), encoding="utf8")).hexdigest()
    return f"https://www.gravatar.com/avatar/{mailhash}.jpg?s={size}&d={alt}"


def now():
    return datetime.utcnow().isoformat()


def profiles(site, template, data, **kwargs):
    for entry in data:
        page_name = profile_slug(entry["name"])
        outdir = os.path.join(site.outpath, page_name)
        dest = os.path.join(outdir, "index.html")
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        template.stream({"entry": entry}).dump(dest, encoding="utf-8")


if __name__ == "__main__":
    data = get_data()
    active = [e for e in data if e["confirm"] == "Understood!"]
    inactive = [e for e in data if e["confirm"] == "INACTIVE"]
    config = tomli.load(open("config.toml"))
    site = Site.make_site(
        filters={
            "markdown": render_markdown,
            "gravatar": gravatar,
            "slug": profile_slug,
        },
        rules=[
            ("redirect.html", partial(profiles, data=inactive)),
            ("profile.html", partial(profiles, data=active)),
        ],
        outpath="public",
        env_globals={"data": list(reversed(active)), "site": config, "now": now()},
    )
    site.render()
